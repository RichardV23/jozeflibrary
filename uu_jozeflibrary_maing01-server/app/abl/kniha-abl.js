"use strict";
const Path = require("path");
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/kniha-error.js");

const WARNINGS = {};

class KnihaAbl {
  constructor() {
    this.validator = Validator.load();
    this.dao = DaoFactory.getDao("kniha");
  }

  async create(awid, dtoIn, uuAppErrorMap = {}) {
    // validation of dtoIn - input of cmd
    let validationResult = this.validator.validate("knihaCreateDtoInType", dtoIn);

    // write to uuAppErrorMap result of validation
    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Get.InvalidDtoIn
    );

    let knihaDtoOut;
    try {
      knihaDtoOut = await this.dao.create({ ...dtoIn, awid });
    } catch (e) {
      throw new Errors.Create.JokeCreateFailed({ uuAppErrorMap }, e);
    }

    return {
      ...knihaDtoOut,
      uuAppErrorMap,
    };
  }

  async update(awid, dtoIn, uuAppErrorMap = {}) {
    // validation of dtoIn - input of cmd
    let validationResult = this.validator.validate("knihaUpdateDtoInType", dtoIn);

    // write to uuAppErrorMap result of validation
    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Get.InvalidDtoIn
    );

    // load kniha from database by id from dtoIn
    let kniha = await this.dao.get(awid, dtoIn.id);

    // if kniha does not exist (was not found in database)
    if (!kniha) {
      throw new Errors.Update.JokeDoesNotExist({ uuAppErrorMap }, { knihaId: dtoIn.id });
    }

    let knihaDtoOut;
    try {
      // call dao method update to edit your joke in database
      knihaDtoOut = await this.dao.update({ ...dtoIn, awid });
    } catch (e) {
      // throw an error if something goes wrong during editing kniha in database
      throw new Errors.Create.knihaCreateFailed({ uuAppErrorMap }, e);
    }

    // return updated kniha
    return {
      ...knihaDtoOut,
      uuAppErrorMap,
    };
  }

  async get(awid, dtoIn, uuAppErrorMap = {}) {
    // validation of dtoIn - input of cmd
    let validationResult = this.validator.validate("knihaGetDtoInType", dtoIn);

    // write to uuAppErrorMap result of validation
    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Get.InvalidDtoIn
    );

    // load kniha from database by id from dtoIn
    let kniha = await this.dao.get(awid, dtoIn.id);

    // if kniha does not exist (was not found in database)
    if (!kniha) {
      throw new Errors.Get.knihaDoesNotExist({ uuAppErrorMap }, { knihaId: dtoIn.id });
    }

    // return found kniha
    return {
      ...kniha,
      uuAppErrorMap,
    };
  }

  async remove(awid, dtoIn, uuAppErrorMap = {}) {
    // validation of dtoIn - input of cmd
    let validationResult = this.validator.validate("knihaRemoveDtoInType", dtoIn);

    // write to uuAppErrorMap result of validation
    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Get.InvalidDtoIn
    );

    // load kniha from database by id from dtoIn
    let kniha = await this.dao.get(awid, dtoIn.id);

    // if kniha does not exist (was not found in database)
    if (!kniha) {
      throw new Errors.Remove.knihaDoesNotExist({ uuAppErrorMap }, { knihaId: dtoIn.id });
    }

    try {
      // call dao method remove to delete your kniha from database
      await this.dao.remove({ ...dtoIn, awid });
    } catch (e) {
      // throw an error if something goes wrong during removing joke from database
      throw new Errors.Create.knihaCreateFailed({ uuAppErrorMap }, e);
    }

    return {
      uuAppErrorMap,
    };
  }
}

module.exports = new KnihaAbl();
