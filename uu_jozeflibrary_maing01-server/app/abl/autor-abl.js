"use strict";
const Path = require("path");
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/autor-error.js");

const WARNINGS = {
  unsupportedKeys: {
    CODE: `${Errors.Create.UC_CODE}unsupportedKeys`,
  },
};

class AutorAbl {
  constructor() {
    this.validator = Validator.load();
    this.dao = DaoFactory.getDao("zoznam");
    this.BookDao = DaoFactory.getDao("kniha");
  }

  async create(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("autorCreateDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Create.InvalidDtoIn
    );
    let autorResult;
    try {
      autorResult = await this.dao.create({ ...dtoIn, awid });
    } catch (e) {
      throw new Errors.Create.AutorCreateFailed({ uuAppErrorMap }, e);
    }
    return {
      ...autorResult,
      uuAppErrorMap,
    };
  }
  async get(awid, dtoIn, uuAppErrorMap = {}) {
    let validationResult = this.validator.validate("autorGetDtoInType", dtoIn);

    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.code,
      Errors.Create.InvalidDtoIn
    );
    let autor = await this.dao.get(awid, dtoIn.id);

    if (!autor) {
      throw new Errors.Update.autorDoesNotExist({ uuAppErrorMap }, { autorId: dtoIn.id });
    }
    return {
      ...autor,
      uuAppErrorMap,
    };
  }
  async update(awid, dtoIn, uuAppErrorMap = {}) {
    // validation of dtoIn - input of cmd
    let validationResult = this.validator.validate("autorUpdateDtoInType", dtoIn);

    // write to uuAppErrorMap result of validation
    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Get.InvalidDtoIn
    );

    let autor = await this.dao.get(awid, dtoIn.id);

    if (!autor) {
      throw new Errors.Update.JokeDoesNotExist({ uuAppErrorMap }, { jokeId: dtoIn.id });
    }

    let autorDtoOut;
    try {
      autorDtoOut = await this.dao.update({ ...dtoIn, awid });
    } catch (e) {
      throw new Errors.Create.autorCreateFailed({ uuAppErrorMap }, e);
    }

    return {
      ...autorDtoOut,
      uuAppErrorMap,
    };
  }
  async remove(awid, dtoIn, uuAppErrorMap = {}) {
    // validation of dtoIn - input of cmd
    let validationResult = this.validator.validate("autorRemoveDtoInType", dtoIn);

    // write to uuAppErrorMap result of validation
    uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.unsupportedKeys.CODE,
      Errors.Get.InvalidDtoIn
    );

    // load autor from database by id from dtoIn
    let autor = await this.dao.get(awid, dtoIn.id);

    // if autor does not exist (was not found in database)
    if (!autor) {
      throw new Errors.Remove.autorDoesNotExist({ uuAppErrorMap }, { jokeId: dtoIn.id });
    }

    try {
      await this.dao.remove({ ...dtoIn, awid });
    } catch (e) {
      throw new Errors.Create.autorCreateFailed({ uuAppErrorMap }, e);
    }

    return {
      uuAppErrorMap,
    };
  }
}
module.exports = new AutorAbl();
