"use strict";

const ZoznamMainUseCaseError = require("./zoznam-main-use-case-error.js");
const KNIHA_ERROR_PREFIX = `${ZoznamMainUseCaseError.ERROR_PREFIX}kniha/`;

const Create = {
  UC_CODE: `${KNIHA_ERROR_PREFIX}create/`,
  InvalidDtoIn: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}'invalidDtoIn`;
      this.message = "Invalid input data";
    }
  },
  KnihaCreateFailed: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}knihaCreateFailed`;
      this.message = "Creating kniha by DAO method create failed.";
    }
  },
};
const Update = {
  UC_CODE: `${KNIHA_ERROR_PREFIX}update/`,

  knihaDoesNotExist: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Update.UC_CODE}knihaDoesNotExist`;
      this.message = "kniha with given id does not exist.";
    }
  },
};

const Get = {
  UC_CODE: `${KNIHA_ERROR_PREFIX}get/`,

  knihaDoesNotExist: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}knihaDoesNotExist`;
      this.message = "kniha with given id does not exist.";
    }
  },
};
const list = {
  UC_CODE: `${KNIHA_ERROR_PREFIX}list/`,

  knihaDoesNotExist: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}knihaDoesNotExist`;
      this.message = "kniha with given id does not exist.";
    }
  },
};
const Delete = {
  UC_CODE: `${KNIHA_ERROR_PREFIX}delete/`,

  knihaDoesNotExist: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}knihaDoesNotExist`;
      this.message = "kniha with given id does not exist.";
    }
  },
};

module.exports = {
  Create,
  Update,
  Get,
  list,
  Delete,
};
