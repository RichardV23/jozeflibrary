"use strict";

const ZoznamMainUseCaseError = require("./zoznam-main-use-case-error.js");
const AUTOR_ERROR_PREFIX = `${ZoznamMainUseCaseError.ERROR_PREFIX}autor/`;

const Create = {
  UC_CODE: `${AUTOR_ERROR_PREFIX}create/`,

  InvalidDtoIn: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}'invalidDtoIn`;
      this.message = "Invalid input data";
    }
  },
  AutorCreateFailed: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}autorCreateFailed`;
      this.message = "Creating autor by DAO method create failed.";
    }
  },
};
const Update = {
  UC_CODE: `${AUTOR_ERROR_PREFIX}update/`,

  AutorDoesNotExist: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Update.UC_CODE}autorDoesNotExist`;
      this.message = "Autor with given id does not exist.";
    }
  },
};

const Get = {
  UC_CODE: `${AUTOR_ERROR_PREFIX}get/`,

  AutorDoesNotExist: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}autorDoesNotExist`;
      this.message = "autor with given id does not exist.";
    }
  },
};

const Delete = {
  UC_CODE: `${AUTOR_ERROR_PREFIX}delete/`,

  AutorDoesNotExist: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}autorDoesNotExist`;
      this.message = "autor with given id does not exist.";
    }
  },
};
const list = {
  UC_CODE: `${AUTOR_ERROR_PREFIX}list/`,

  AutorDoesNotExist: class extends ZoznamMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Delete.UC_CODE}AutorDoesNotExist`;
    }
  },
};
module.exports = {
  Create,
  Update,
  Delete,
  list,
  Get,
};
