"use strict";
const KnihaAbl = require("../../abl/kniha-abl.js");
const AutorAbl = require("../../abl/autor-abl");

class KnihaController {
  create(ucEnv) {
    return KnihaAbl.create(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
  update(ucEnv) {
    return KnihaAbl.update(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
  get(ucEnv) {
    return KnihaAbl.get(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
  list(ucEnv) {
    return KnihaAbl.remove(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
  delete(ucEnv) {
    return KnihaAbl.remove(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
}

module.exports = new KnihaController();
