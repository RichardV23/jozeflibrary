"use strict";
const AutorAbl = require("../../abl/autor-abl.js");

class AutorController {
  create(ucEnv) {
    return AutorAbl.create(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
  update(ucEnv) {
    return AutorAbl.update(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
  get(ucEnv) {
    return AutorAbl.get(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  remove(ucEnv) {
    return AutorAbl.remove(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
}

module.exports = new AutorController();
