"use strict";
const ZoznamMainAbl = require("../../abl/zoznam-main-abl.js");

class ZoznamMainController {
  init(ucEnv) {
    return ZoznamMainAbl.init(ucEnv.getUri(), ucEnv.getDtoIn(), ucEnv.getSession());
  }

  load(ucEnv) {
    return ZoznamMainAbl.load(ucEnv.getUri(), ucEnv.getSession());
  }

  loadBasicData(ucEnv) {
    return ZoznamMainAbl.loadBasicData(ucEnv.getUri(), ucEnv.getSession());
  }
}

module.exports = new ZoznamMainController();
