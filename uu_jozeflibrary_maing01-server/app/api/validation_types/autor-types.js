/* eslint-disable */
const initDtoInType = shape({
  uuAppProfileAuthorities: uri().isRequired("uuBtLocationUri"),
  uuBtLocationUri: uri(),
  name: uu5String(512),
  sysState: oneOf(["active", "restricted", "readOnly"]),
  adviceNote: shape({
    message: uu5String().isRequired(),
    severity: oneOf(["debug", "info", "warning", "error", "fatal"]),
    estimatedEndTime: datetime(),
  })
});
const autorCreateDtoInType = shape({
 name: string(50).isRequired(),
 year: string(4).isRequired(),
 nationality: string(74).isRequired(),
});


const autorRemoveDtoInType = shape({
  id: id().isRequired(),
});

const autorGetDtoInType = shape({
  id: id().isRequired(),
});

const autorUpdateDtoInType = shape({
  name: string(50).isRequired(),
  year: string(4).isRequired(),
  nationality: string(74).isRequired(),
  id: id().isRequired(),
});


