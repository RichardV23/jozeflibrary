/* eslint-disable */
const initDtoInType = shape({
  uuAppProfileAuthorities: uri().isRequired("uuBtLocationUri"),
  uuBtLocationUri: uri(),
  name: uu5String(512),
  sysState: oneOf(["active", "restricted", "readOnly"]),
  adviceNote: shape({
    message: uu5String().isRequired(),
    severity: oneOf(["debug", "info", "warning", "error", "fatal"]),
    estimatedEndTime: datetime(),
  })
});
const knihaCreateDtoInType = shape({
  name: string(50).isRequired(),
  description: string(100).isRequired(),
  year: string(4).isRequired(),
  pages: string(3).isRequired(),
  autor: string(74).isRequired(),
  id: id().isRequired(),
  language: string(74).isRequired(),

});


const knihaDeleteDtoInType = shape({
  name: string(50).isRequired(),
  description: string(100).isRequired(),
  autor: string(74).isRequired(),
  id: id().isRequired(),

});

const knihaGetDtoInType = shape({
  id: id().isRequired(),
});

const knihaUpdateDtoInType = shape({
  name: string(50).isRequired(),
  description: string(100).isRequired(),
  year: string(4).isRequired(),
  pages: string(3).isRequired(),
  autor: string(74).isRequired(),
  id: id().isRequired(),
  language: string(74).isRequired(),
});

const knihalistDtoInType = shape({
  name: string(50).isRequired(),
  description: string(100).isRequired(),
  year: string(4).isRequired(),
  pages: string(3).isRequired(),
  autor: string(74).isRequired(),
  id: id().isRequired(),
  language: string(74).isRequired(),
});

