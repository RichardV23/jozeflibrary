"use strict";
const { UuObjectDao } = require("uu_appg01_server").ObjectStore;

class zoznamMongo extends UuObjectDao {
  async createSchema() {}

  async create(autor) {
    return await super.insertOne(autor);
  }
  async get(awid, id) {
    let filter = {
      awid: awid,
      id: id,
    };
    return await super.findOne(filter);
  }
  async update(uuObject) {
    let filter = {
      awid: uuObject.awid,
      id: uuObject.id,
    };
    return await super.findOneAndUpdate(filter, uuObject, "NONE");
  }

  async remove(uuObject) {
    let filter = {
      awid: uuObject.awid,
      id: uuObject.id,
    };
    return await super.deleteOne(filter);
  }
}

module.exports = zoznamMongo;
